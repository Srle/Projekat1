﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer3
{
    public class MethodClass1
    {
        /*
	    Pozeljno je iznad funkcije napisati kratak komentar
	    kojim se objasnjava programska logika i svrha funkcije
	    */
        //	ispis "Pozdrav svima" teksta

        public static void Pozdrav()
        {
            Console.WriteLine("Pozdrav svima");
        }

        public static void Main(String[] args)
        {
            Pozdrav();

            //citanje argumenata komandne linije
            //parametri podeseni na sledeći način: Project > Properties > Debug > Start Options > Command line arguments
            Console.WriteLine("Ukupno je prosleđeno " + args.Length + " parametara.");
            if (args.Length > 0)
            {
                Console.Write("Parametri su:");
                for (int i = 0; i < args.Length; i++)
                {
                    Console.Write(" " + args[i]);
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
