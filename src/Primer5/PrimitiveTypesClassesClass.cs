﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer5
{
    class PrimitiveTypesClassesClass
    {
        public static void Main(String[] args)
        {
            Console.WriteLine("Za svaki primitivni tip postoji i odgovarajuća klasa");

            //int = Int32
            Int32 intObjVrednost = new Int32();
            int intVrenost = 0;

            Console.WriteLine("Vrednosti promenljivih: "+intObjVrednost + "   "+intVrenost);

            Double b = 13.3;
            Boolean c = true;
           
            Console.WriteLine("Bez obzira na način kreiranja variable (int ili Int32) istog su tipa:\n  " + intVrenost.GetType() + " - " + intObjVrednost.GetType());

            //parsiranje teksta u određeni tip
            double pi = Double.Parse("3.14");
            Console.WriteLine("Parsirana vrednost pi je: " + pi);

            //greska - pogresno prosledjen tip u string zapisu
            //intVrenost = Int32.Parse("3.14");

            intObjVrednost++;
            Console.WriteLine("Izmenjena vrednost " + intObjVrednost);

            //Boxing i Unboxing

            int intVal = 123;

            // Boxing - kopira vrednost intVal u objekat intObj (implicitno kastovanje)
            object intObj = intVal;
            // Unboxing - kopira vrednost intObj u intVal (eksplicitno kastovanje)
            intVal = (int)intObj;

            // Promena vrednosti intVal ne i objekta intObj
            intVal = 456;

            //greska - intObj sadrži referencu na datu vrednost datog tipa ali se ne može ponašati kao Int32
            //intObj++;

            System.Console.WriteLine("Vrednosni tip = {0}", intVal);
            System.Console.WriteLine("Adresni tip = {0}", intObj);
            
            Console.ReadKey();

        }
    }
}
