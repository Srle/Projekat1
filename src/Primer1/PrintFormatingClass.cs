﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Modul1Termin02.Primer1
{
    class PrintFormatingClass
    {
        public static void Main(string[] args)
        {
            int broj = 1;
            string poruka = "Neki drugi ispis";
            Console.WriteLine("Poruka br. {0} glasi: {1}", broj, poruka);

            int iCena = 123456; float fCena = 123456.789f; double dbCena = 123456.789; decimal dCena = 123456.789m;

            #region FormatiranjeIspisa

            Console.WriteLine("2. Cena artikla iznosi: \n int: \n\t{0:c} \n float: \n\t{1:c} \n decimal: \n\t{2:c} \n", iCena, fCena, dCena);

            //po default-u je 15 cifara preciznosti, a sa opcijom g17 se prosiruje preciznost double vrednosti na maksimalnih 17 cifara
            //ili koristiti "r" za automatsko podesavanje preciznosti
            Console.WriteLine("3. Cena artikla iznosi: \n\t{0:d} \n\t{0:n} \n\t{1:0.000} \n\t{1:g17}", iCena, fCena);

            Console.WriteLine("4. Cena artikla iznosi: \n int: \n\t{0:g} {0:n}  \n float: \n\t{1:g} \n decimal: \n\t{2:g} \n", iCena, fCena, dCena);

            Console.WriteLine("5. Cena artikla iznosi: \n\t{0:r} \n\t{1:r} \n\t{2:r} ", fCena, (double)fCena, dbCena);

            Console.WriteLine("6. Cena artikla iznosi: \n\t{0,-15:(#).##}!", dCena);

            Console.WriteLine("7. Cena artikla iznosi: \n\t{0:0,0.0}!", dCena);

            Console.WriteLine("8. Poruka glasi: {0,20}!", poruka);

            Console.WriteLine("\n\tFormatiranje ispisa\n\n");

            Console.WriteLine("Valuta: \n\t{0:c} \n\t{1:c} \n\t{2:c}", dCena, dbCena, iCena);

            Console.WriteLine("Decimalni (celobrojni): \n\t{2:d} \n\t{2:d} \n\t{2:d}", dCena, dbCena, iCena); //greska za decimal i double. Zato je ostavljen br. 2

            Console.WriteLine("Naučni: \n\t{0:e} \n\t{1:e} \n\t{2:e}", dCena, dbCena, iCena);

            Console.WriteLine("Fiksna decimalna tačka: \n\t{0:f} \n\t{1:f4} \n\t{2:f2}", dCena, dbCena, iCena);

            Console.WriteLine("Generalni: \n\t{0:g} \n\t{1:g} \n\t{2:g}", dCena, dbCena, iCena);

            Console.WriteLine("Brojni sa zarezima za hiljade: \n\t{0:n} \n\t{1:n} \n\t{2:n}", dCena, dbCena, iCena);

            Console.WriteLine("Procentni: \n\t{0:p} \n\t{1:p} \n\t{2:p}", dCena, dbCena, iCena);

            Console.WriteLine("Round-trip: \n\t{1:r} \n\t{1:r} \n\t{1:r}", dCena, dbCena, iCena);  //greska za decimal i int. Zato je ostavljen br. 1

            Console.WriteLine("Heksadecimalni: \n\t{2:x8} \n\t{2:x8} \n\t{2:x8}", dCena, dbCena, iCena); //greska za decimal i double. Zato je ostavljen br. 2

            fCena = 1.234567f;

            Console.WriteLine("Rezervacija mesta za nule: \n\t{0:00.00000} \n\t{1:00.00000}", dbCena, fCena);

            Console.WriteLine("Rezervacija mesta za cifre: \n\t{0:#####.##} \n\t{1:#####.##}", dbCena, fCena);

            Console.WriteLine("Decimalna tačka: \n\t{0:0.0} \n\t{1:0.00}", dbCena, fCena);

            Console.WriteLine("Separator hiljada: \n\t{0:0,00} \n\t{1:0,00}", dbCena, fCena);

            Console.WriteLine("Skaliranje brojeva: \n\t{0:0,.0} \n\t{1:0,.0}", dbCena, fCena);

            Console.WriteLine("Procenat [%]: \n\t{0:0%} \n\t{1:0%}", dbCena, fCena);

            Console.WriteLine("Rezervisanje mesta eksponenta: \n\t{0:00.00e+00} \n\t{1:00.00e+0}", dbCena, fCena);

            Console.WriteLine("Escape karakter: \n\t{0:\\t0#-###-###\\t}  \n\t{1:\\t0#-###-###\\t}", dbCena, fCena);

            Console.WriteLine("Umetanje stringa u format: \n\t{0:[# m\u00B2]}  \n\t{1:[# m\u00B2]}", dbCena, fCena);

            Console.WriteLine("Separator cekcije: \n\t{0:$#,##0.00;(-#,##0.00);Nula}  \n\t{1:$#,##0.00;(-#,##0.00);Nula}", dbCena, fCena);
            #endregion

            #region MicrosoftFormatExamples
            // izvor: msdn.microsoft.com
            // The example displays the following output to the console:
            //       Formatting of 1054.32179:
            //          C:                     $1,054.32
            //          C0:                    $1,054
            //          C1:                    $1,054.3
            //          C2:                    $1,054.32
            //          C3:                    $1,054.322
            //       
            //          E:                     1.054322E+003
            //          E0:                    1E+003
            //          E1:                    1.1E+003
            //          E2:                    1.05E+003
            //          E3:                    1.054E+003
            //       
            //          e:                     1.054322e+003
            //          e0:                    1e+003
            //          e1:                    1.1e+003
            //          e2:                    1.05e+003
            //          e3:                    1.054e+003
            //       
            //          F:                     1054.32
            //          F0:                    1054
            //          F1:                    1054.3
            //          F2:                    1054.32
            //          F3:                    1054.322
            //       
            //          G:                     1054.32179
            //          G0:                    1054.32179
            //          G1:                    1E+03
            //          G2:                    1.1E+03
            //          G3:                    1.05E+03
            //       
            //          N:                     1,054.32
            //          N0:                    1,054
            //          N1:                    1,054.3
            //          N2:                    1,054.32
            //          N3:                    1,054.322
            //       
            //          P:                     105,432.18 %
            //          P0:                    105,432 %
            //          P1:                    105,432.2 %
            //          P2:                    105,432.18 %
            //          P3:                    105,432.179 %
            //       
            //          R:                     1054.32179
            //          #,000.000:             1,054.322
            //          0.###E-000:            1.054E003
            //          000,000,000,000.00###: 000,000,001,054.32179
            //       
            //       Formatting of -195489100.8377:
            //          C:                     ($195,489,100.84)
            //          C0:                    ($195,489,101)
            //          C1:                    ($195,489,100.8)
            //          C2:                    ($195,489,100.84)
            //          C3:                    ($195,489,100.838)
            //       
            //          E:                     -1.954891E+008
            //          E0:                    -2E+008
            //          E1:                    -2.0E+008
            //          E2:                    -1.95E+008
            //          E3:                    -1.955E+008
            //       
            //          e:                     -1.954891e+008
            //          e0:                    -2e+008
            //          e1:                    -2.0e+008
            //          e2:                    -1.95e+008
            //          e3:                    -1.955e+008
            //       
            //          F:                     -195489100.84
            //          F0:                    -195489101
            //          F1:                    -195489100.8
            //          F2:                    -195489100.84
            //          F3:                    -195489100.838
            //       
            //          G:                     -195489100.8377
            //          G0:                    -195489100.8377
            //          G1:                    -2E+08
            //          G2:                    -2E+08
            //          G3:                    -1.95E+08
            //       
            //          N:                     -195,489,100.84
            //          N0:                    -195,489,101
            //          N1:                    -195,489,100.8
            //          N2:                    -195,489,100.84
            //          N3:                    -195,489,100.838
            //       
            //          P:                     -19,548,910,083.77 %
            //          P0:                    -19,548,910,084 %
            //          P1:                    -19,548,910,083.8 %
            //          P2:                    -19,548,910,083.77 %
            //          P3:                    -19,548,910,083.770 %
            //       
            //          R:                     -195489100.8377
            //          #,000.000:             -195,489,100.838
            //          0.###E-000:            -1.955E008
            //          000,000,000,000.00###: -000,195,489,100.8377
            //       
            //       Formatting of 1.0437E+21:
            //          C:                     $1,043,700,000,000,000,000,000.00
            //          C0:                    $1,043,700,000,000,000,000,000
            //          C1:                    $1,043,700,000,000,000,000,000.0
            //          C2:                    $1,043,700,000,000,000,000,000.00
            //          C3:                    $1,043,700,000,000,000,000,000.000
            //       
            //          E:                     1.043700E+021
            //          E0:                    1E+021
            //          E1:                    1.0E+021
            //          E2:                    1.04E+021
            //          E3:                    1.044E+021
            //       
            //          e:                     1.043700e+021
            //          e0:                    1e+021
            //          e1:                    1.0e+021
            //          e2:                    1.04e+021
            //          e3:                    1.044e+021
            //       
            //          F:                     1043700000000000000000.00
            //          F0:                    1043700000000000000000
            //          F1:                    1043700000000000000000.0
            //          F2:                    1043700000000000000000.00
            //          F3:                    1043700000000000000000.000
            //       
            //          G:                     1.0437E+21
            //          G0:                    1.0437E+21
            //          G1:                    1E+21
            //          G2:                    1E+21
            //          G3:                    1.04E+21
            //       
            //          N:                     1,043,700,000,000,000,000,000.00
            //          N0:                    1,043,700,000,000,000,000,000
            //          N1:                    1,043,700,000,000,000,000,000.0
            //          N2:                    1,043,700,000,000,000,000,000.00
            //          N3:                    1,043,700,000,000,000,000,000.000
            //       
            //          P:                     104,370,000,000,000,000,000,000.00 %
            //          P0:                    104,370,000,000,000,000,000,000 %
            //          P1:                    104,370,000,000,000,000,000,000.0 %
            //          P2:                    104,370,000,000,000,000,000,000.00 %
            //          P3:                    104,370,000,000,000,000,000,000.000 %
            //       
            //          R:                     1.0437E+21
            //          #,000.000:             1,043,700,000,000,000,000,000.000
            //          0.###E-000:            1.044E021
            //          000,000,000,000.00###: 1,043,700,000,000,000,000,000.00
            //       
            //       Formatting of -1.0573E-05:
            //          C:                     $0.00
            //          C0:                    $0
            //          C1:                    $0.0
            //          C2:                    $0.00
            //          C3:                    $0.000
            //       
            //          E:                     -1.057300E-005
            //          E0:                    -1E-005
            //          E1:                    -1.1E-005
            //          E2:                    -1.06E-005
            //          E3:                    -1.057E-005
            //       
            //          e:                     -1.057300e-005
            //          e0:                    -1e-005
            //          e1:                    -1.1e-005
            //          e2:                    -1.06e-005
            //          e3:                    -1.057e-005
            //       
            //          F:                     0.00
            //          F0:                    0
            //          F1:                    0.0
            //          F2:                    0.00
            //          F3:                    0.000
            //       
            //          G:                     -1.0573E-05
            //          G0:                    -1.0573E-05
            //          G1:                    -1E-05
            //          G2:                    -1.1E-05
            //          G3:                    -1.06E-05
            //       
            //          N:                     0.00
            //          N0:                    0
            //          N1:                    0.0
            //          N2:                    0.00
            //          N3:                    0.000
            //       
            //          P:                     0.00 %
            //          P0:                    0 %
            //          P1:                    0.0 %
            //          P2:                    0.00 %
            //          P3:                    -0.001 %
            //       
            //          R:                     -1.0573E-05
            //          #,000.000:             000.000
            //          0.###E-000:            -1.057E-005
            //          000,000,000,000.00###: -000,000,000,000.00001
            #endregion

            Console.ReadKey();
        }
    }
}
